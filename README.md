# macOs reboot

Mon install MacOS en cli.

## Général
- Transfer "Session" Apple keychain
- Transfer ~/.ssh/ folder

## Perso
- bibliothèque Calibre (export/import)
- bibliothèque iMovie/Photos (copy/paste directory)
- Canon EOS Link (M50)
- Sonos —> https://www.sonos.com/fr-fr/home

## Pro

- Sequel Pro > Servers + vérifier pwd dans la keychain + export/import favorites query 
- RDP > computer list + vérifier pwd dans la keychain
- Transfert  credentials aws + .aws directory
- DB Browser for SQLite ?

***

## Minimal setup

```sh
# Xcode
xcode-select --install

# Brew !
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Auto update outdated cask installed apps
brew tap buo/cask-upgrade

# Is everything ok ?
brew update;brew upgrade;brew doctor 
```

### Setup iTerm

iTerm2 word jumps :  
—> iTerm > Preferences > Profiles > Keys > Load Presets… select Natural Text Editing.

### zsh

```sh
brew tap homebrew/cask-fonts # deal with font throught brew
brew install font-inter # https://github.com/rsms/inter / Set font size & add ligatures to vscode
```

My terminal is so cute ^^
- https://ohmyz.sh/
- https://github.com/romkatv/powerlevel10k

### Alias 

```sh
# Network commands
alias ip="curl -s http://checkip.dyndns.org/ | sed 's/[a-zA-Z<>/ :]//g'"
alias localip="ifconfig | grep \"inet \" | grep -Fv 127.0.0.1 | awk '{print $2}'"
alias dnsflush="sudo killall -HUP mDNSResponder;" # Flush Monterey DNS cache

# Secure random password, expects length. Example: mkpass 16
alias mkpass='openssl rand -base64'

# brew aliases
alias brewupdate='brew update && brew upgrade && brew cu --all --yes --cleanup && mas upgrade && brew cleanup'
alias brewlist='brew bundle dump --force --describe --file=~/Downloads/Brewfile'

# some more aliases
alias week='date +%V'

# extension aliases
alias -s {json,sql,md,yml,yaml}=code
alias -s {pdf,jpg,png,ai,psd,svg}='open -a Preview'

```
## Software installation

Reste à piocher dans la liste :

```sh

# Essentials apps
brew install --cask firefox
brew install --cask google-chrome
brew install --cask iterm2
brew install --cask raycast
brew install --cask microsoft-office
brew install the-unarchiver
brew install --cask tad
brew install --cask mounty # Re-mounts write-protected NTFS volumes
brew install --cask barrier # Open-source KVM software
brew install --cask qlmarkdown # QuickLook generator for Markdown files
brew install mac-mouse-fix #Mouse utility to add gesture functions and smooth scrolling to 3rd party mice

# windows management
brew install --cask alt-tab
brew install --cask moom # /!\ Licence file

# Com
brew install --cask teamviewer
brew install microsoft-teams
brew install slack
brew install whatsapp
brew install --cask zoom
brew install --cask microsoft-remote-desktop

# Auth
brew install authy
brew install --cask bitwarden
brew install bitwarden-cli

# Dev
brew install docker
brew install visual-studio-code
brew install notion
brew install --cask ngrok

#UX
brew install --cask figma

# Personal stuffs
brew install --cask itsycal
brew install --cask remarkable
brew install --cask hiddenbar
brew install calibre
brew install spotify
brew install vlc

# Record apps
brew install --cask shottr #Skitch replacement
brew install --cask recordit

# Cloud app
brew install dropbox
brew install google-drive
brew install --cask onedrive

# Cli 
brew install iterm2
brew install --cask fig

# Remove last login datetime on cli / MacOs 
touch ~/.hushlogin

# Cli binaries
brew install the_silver_searcher        # ag —> Code-search similar to (faster than) ack 
brew install awscli     # Official Amazon AWS command-line interface
brew install bat        # Clone of cat(1) with syntax highlighting and Git integration
brew install bmon       # Interface bandwidth monitor
brew install broot      # New way to see and navigate directory trees
brew install brotli     # Generic-purpose lossless compression algorithm by Google
brew install cairo      # Vector graphics library with cross-device output support
brew install cheat      # Create and view interactive cheat sheets for *nix commands
brew install croc       # Securely send things from one computer to another
brew install csvkit     # Suite of command-line tools for converting to and working with CSV, read more —> https://csvkit.readthedocs.io/
brew install dog        # Command-line DNS client
brew install duf        # Disk Usage/Free Utility - a better 'df' alternative
brew install dust       # More intuitive version of du in rust
brew install exa        # Modern replacement for 'ls'
brew install ffmpeg     # Play, record, convert, and stream audio and video
brew install glances    # Alternative to top/htop
brew install gping      # Ping, but with a graph
brew install gtop       # System monitoring dashboard for terminal
brew install iperf3     # Update of iperf: measures TCP, UDP, and SCTP bandwidth
brew install jq         # Lightweight and flexible command-line JSON processor
brew install ncdu       # NCurses Disk Usage
brew install rdfind     # Find duplicate files based on content (NOT file names)
brew install rename     # Perl-powered file rename script with many helpful built-ins
brew install tldr       # Simplified and community-driven man pages
brew install visidata   # Terminal utility for exploring and arranging tabular data
brew install watch      # Executes a program periodically, showing output fullscreen
brew install wget       # Internet file retriever

# Let's play
brew install --cask nvidia-geforce-now
brew install --cask steam

# App Store
brew install mas # Mac App Store as a CLI
mas install 937984704 # Amphetamine
mas install 569502669 # Igeneration
mas install 1274495053 # Microsoft to-do
```
